# gnome-text-editor

Simple text editor that focuses on session management

https://gitlab.gnome.org/GNOME/gnome-text-editor

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/gnome-packages/gnome-text-editor.git
```

